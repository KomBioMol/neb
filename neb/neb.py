import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import RegularGridInterpolator
from copy import deepcopy
import sys


class NEB:
    def __init__(self, infile, init, end, npts=50, ndim=2, intermediate=None, loose_ends=True,
                 k=500):
        self.grid, self.gridsize, self.profile = 3 * [None]
        self.ndim = ndim
        self.unit = 'kJ'
        self.loose = loose_ends
        if not intermediate:
            self.pts = np.linspace(init, end, npts)
        elif isinstance(intermediate[0], float):
            self.pts = np.concatenate((np.linspace(init, intermediate, npts//2)[:-1],
                                       np.linspace(intermediate, end, npts - npts//2)))
        else:
            self.pts = np.concatenate([np.linspace(init, intermediate[0], npts//len(intermediate))[:-1]] +
                                      [np.linspace(intermediate[i], intermediate[i+1], npts//len(intermediate))[:-1]
                                       for i in range(len(intermediate)-1)] +
                                      [np.linspace(intermediate[-1], end, npts//len(intermediate))])
        self.pts_conv = []
        self.k = k
        self.k_ang = 1
        self.dx = 0.0001
        self.dvers = np.eye(ndim) * self.dx
        self.to_matrix(infile)
        self.ranges = 0.25*np.array([np.max(self.grid[x]) - np.min(self.grid[x]) for x in range(self.ndim)])
        self.interpd = RegularGridInterpolator(self.grid, self.profile)
        self.optimize()
        
    def to_matrix(self, file):
        datafile = np.loadtxt(file, usecols=range(self.ndim+1))
        self.grid = [np.array(sorted(list(set(datafile[:, x])))) for x in range(self.ndim)]
        self.gridsize = [len(x) for x in self.grid[::-1]]
        self.profile = datafile[:, -1].reshape(*self.gridsize).T
        self.profile -= np.min(self.profile)
        
    def plot(self, levels=7, cmap='viridis', convergence=False, axis=None):
        """
        Plots the n-dimensional FES (currently only 2D is supported)
        along with the lowest-free energy path. Optionally, convergence
        of the path can be shown along with the result.
        :param levels: int, how many levels should be plotted in the contourmap
        :param cmap: str, which colormap (matplotlib-compatible) to use
        :param convergence: bool, whether to show convergence of the path
        :return: None
        """
        if axis is None:
            fig, ax = plt.subplots()
        else:
            ax = axis
        if self.ndim == 2:
            ax.contour(self.grid[0], self.grid[1], self.profile.T, colors='k', levels=levels)
            ax.contourf(self.grid[0], self.grid[1], self.profile.T, cmap=cmap, levels=levels)
            ax.legend()
        ax.scatter(*self.pts.T, c='k', s=25)
        ax.plot(*self.pts.T, c='k')
        if convergence:
            for n, p in enumerate(self.pts_conv):
                ax.plot(*p.T, c='k', alpha=(n+0.5)/len(self.pts_conv))
        if axis is None:
            plt.show()

    def plot3d(self, maxv=30, nlevel=9):
        from mayavi import mlab
        mgr = np.mgrid[self.grid[0][0]:self.grid[0][-1]:len(self.grid[0])*1j,
              self.grid[1][0]:self.grid[1][-1]:len(self.grid[1])*1j,
              self.grid[2][0]:self.grid[2][-1]:len(self.grid[2])*1j]
        mlab.contour3d(*mgr, self.profile, contours=list(np.linspace(0, maxv, nlevel)), opacity=0.3)
        mlab.plot3d([pt[0] for pt in self.pts], [pt[1] for pt in self.pts],
                    [pt[2] for pt in self.pts], [self.interpd(x)[0] for x in self.pts])
        mlab.show()

        
    def plot1d(self, axis=None):
        """
        Plots the 1D lowest-free energy profile along the reaction coordinate,
        defined in the range from 0 to 1
        :return: None
        """
        segment_lengths = np.linalg.norm(self.pts[1:] - self.pts[:-1], axis=1)
        x_coords = np.concatenate(([0], np.cumsum(segment_lengths)))
        x_coords /= np.sum(segment_lengths)
        if axis is None:
            fig, ax = plt.subplots()
        else:
            ax = axis
        ax.plot(x_coords, [self.interpd(x) for x in self.pts])
        ax.set_xlabel('Reaction progress')
        ax.set_ylabel('Free energy [{}/mol]'.format(self.unit))
        if axis is None:
            plt.show()
        np.savetxt('1d_profile_{}.dat'.format(self.unit), np.array([self.interpd(x) for x in self.pts]).T, fmt='%10.4f')
    
    def optimize(self, n=200):
        for i in range(n):
            forces = []
            for npt in range(len(self.pts)):
                ter = True if npt in [0, len(self.pts)-1] else False
                forces.append(self.grad_pt(npt, ter))
            for npt in range(1, len(self.pts)-1):
                forces[npt] += self.grad_ang(self.pts[npt-1], self.pts[npt], self.pts[npt+1])
            for npt in range(len(self.pts)):
                self.pts[npt] += 0.1 * forces[npt] * 0.985**i
            if i%20 == 0:
                self.pts_conv.append(deepcopy(self.pts))
    
    def grad_pt(self, npt, terminal):
        pt = self.pts[npt]
        try:
            grad = np.array([(self.interpd(pt + dver) - self.interpd(pt)) / self.dx for dver in self.dvers])
        except ValueError:
            print("Point {} is out of the interpolation range".format(pt))
        grad = grad.reshape(-1)
        if terminal:
            if self.loose:
                return -self.ranges * grad/np.linalg.norm(grad)
            else:
                return 0 * grad
        next = self.pts[npt+1]
        prev = self.pts[npt-1]
        tang = next - prev
        tang /= np.linalg.norm(tang)
        f_perp = -grad + np.dot(grad, tang) * tang
        f_tang = np.dot(self.k * (next - 2*pt + prev), tang) * tang
        f = f_perp + f_tang
        return self.ranges * f/np.linalg.norm(f)
    
    def grad_ang(self, pt1, pt2, pt3):
        v1 = pt1 - pt2
        v2 = pt3 - pt2
        arg = np.dot(v1,v2)/(np.linalg.norm(v1) * np.linalg.norm(v2))
        ang = np.arccos(arg)
        diff_ang = np.pi - ang
        frc = self.k_ang * diff_ang * (v1 + v2)
        return frc if not any(np.isnan(frc)) else np.array([0 for _ in frc])
    
    def kj2kcal(self):
        """
        Converts the FES from kJ/mol to kcal/mol
        :return: None
        """
        if self.unit != 'kcal':
            self.profile /= 4.184
        self.unit = 'kcal'
        
    def kcal2kj(self):
        """
        Converts the FES from kcal/mol to kJ/mol
        :return: None
        """
        if self.unit != 'kJ':
            self.profile *= 4.184
        self.unit = 'kJ'
        
    def increase_precision(self):
        """
        Adds another 500 rounds of optimization
        if initial convergence is not satisfactory
        :return: None
        """
        self.optimize(n=500)


def external_run():
    try:
        _ = sys.argv[3]
    except KeyError:
        print("Not enough parameters. Provide (1) the data file, "
              "(2) coordinates of the starting point as a tuple '(X0, Y0)', "
              "(3) coordinates of the endpoint as a tuple '(X1, Y1)'.")
        sys.exit(1)
    neb = NEB(sys.argv[1], eval(sys.argv[2]), eval(sys.argv[3]))
    neb.kj2kcal()
    fig, ax = plt.subplots(2)
    neb.plot(axis=ax[0])
    neb.plot1d(axis=ax[1])
    plt.show()


if __name__ == "__main__":
    external_run()
