NEB introduces the Nudged Elastic Band formalism (see e.g. https://theory.cm.utexas.edu/henkelman/pubs/jonsson98_385.pdf for theory and motivation)
to facilitate the location of 1-dimensional lowest-energy paths on complex
high-dimensional free or potential energy hypersurfaces.

It currently suports several features:
* plotting of the resulting 1D profiles
* customized plotting of the reaction coordinate on 2D profiles (3D is to come)
* adding intermediate points to enforce alternative pathways
* support for arbitrary number of dimensions
 

Data should be provided in the following format:
```
x1_value1 x2_value1 ... energy_value11
x1_value1 x2_value2 ... energy_value12
...
x1_value1 x2_valueN ... energy_value1N
x1_value2 x2_value1 ... energy_value21
x1_value2 x2_value2 ... energy_value22
...
x1_value2 x2_valueN ... energy_value2N
...
x1_valueM x2_valueN ... energy_valueMN
```

Sample usage with 2D data:

```
from neb import NEB
n = NEB('fes_O8G5.dat', init=(3.5,2), end=(2.5,1))
n.kj2kcal()
n.plot()
n.plot1d()
```

If `neb` is installed locally, e.g., via pip:

```
pip install .
```

the above code can be called as a simple command line tool:

`neb fes_O8G5.dat "(3.5,2)" "(2.5,1)"`