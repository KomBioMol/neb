from setuptools import setup

setup(name='neb',
      version='0.1',
      description='A simple nudged elastic band (NEB) implementation to trace and plot lowest free energy paths on multidimensional free energy (hyper-)surfaces',
      url='https://gitlab.com/KomBioMol/neb',
      author='Milosz Wieczor',
      author_email='milafternoon@gmail.com',
      license='GNU GPLv3',
      packages=['neb'],
      entry_points={'console_scripts': ['neb = neb.neb:external_run']},
      python_requires='>=3.4',
      zip_safe=False)
